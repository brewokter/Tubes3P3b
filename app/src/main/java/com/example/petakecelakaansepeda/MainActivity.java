package com.example.petakecelakaansepeda;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button button_list_data, button_list_data_page, button_list_data_type,
            button_bantuan, button_tentang, button_exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_list_data = findViewById(R.id.button_list_data);
        button_list_data_page = findViewById(R.id.button_list_data_page);
        button_list_data_type = findViewById(R.id.button_list_data_type);
        button_bantuan = findViewById(R.id.button_bantuan);
        button_tentang = findViewById(R.id.button_tentang);
        button_exit = findViewById(R.id.button_exit);

        button_list_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ListActivity.class));
                finish();
            }
        });

        button_list_data_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ListPageActivity.class));
                finish();
            }
        });

        button_list_data_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ListIncidentTypeActivity.class));
                finish();
            }
        });

        button_bantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, BantuanActivity.class));
                finish();
            }
        });

        button_tentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, TentangActivity.class));
                finish();
            }
        });

        button_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
}