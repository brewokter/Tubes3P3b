package com.example.petakecelakaansepeda;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolderApi {

    @GET("incidents")
    Call<List<GetSetData>> getPosts();
}
