package com.example.petakecelakaansepeda;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListPageActivity extends AppCompatActivity {

    AdapterData adapterData;
    List<GetSetData> getSetList;
    RecyclerView recyclerView;
    Button button_back, button_next;
    TextView id_page;

    Session session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_page);

        getSetList = new ArrayList<>();
        session = new Session(this);

        id_page = findViewById(R.id.id_page);
        id_page.setText("Page " + session.getPage());
        button_back = findViewById(R.id.button_back);
        if(session.getPage() == 1){
            button_back.setVisibility(View.INVISIBLE);
        }
        button_next = findViewById(R.id.button_next);
        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setPage(session.getPage() + 1);
                startActivity(new Intent(ListPageActivity.this, ListPageActivity.class));
                finish();
            }
        });

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(ListPageActivity.this));
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(ListPageActivity.this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        int itemPosition = recyclerView.getChildLayoutPosition(view);
                        final GetSetData item = getSetList.get(itemPosition);
                        Toast.makeText(ListPageActivity.this, item.getGetSetSource().getHtml_url(), Toast.LENGTH_LONG).show();
                            /*Intent intent = new Intent(ListActivity.this, ListActivity.class);
                            intent.putExtra("id", item.getId());
                            intent.putExtra("url", item.getUrl());
                            startActivity(intent);
                            finish();*/
                    }

                    @Override public void onLongItemClick(View view, int position) {

                    }
                })
        );

        Retrofit retrofit = new Builder()
                .baseUrl("http://bikewise.org/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        JsonPlaceHolderJsonParams jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderJsonParams.class);

        //Call<List<GetSetData>> call = jsonPlaceHolderApi.getPosts();
        Call<JsonResponse> call = jsonPlaceHolderApi.getJson(
                session.getPage()
        );

        call.enqueue(new Callback<JsonResponse>() {
            @Override
            public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                if(!response.isSuccessful()){
                    return;
                }

                JsonResponse posts = response.body();
                getSetList.addAll(Arrays.asList(posts.getBikeArray()));

                adapterData = new AdapterData(getSetList, ListPageActivity.this);
                recyclerView.setAdapter(adapterData);
            }

            @Override
            public void onFailure(Call<JsonResponse> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ListPageActivity.this, MainActivity.class));
        finish();
    }
}
