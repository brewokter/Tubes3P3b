package com.example.petakecelakaansepeda;


import com.google.gson.annotations.SerializedName;

public class GetSetData {

    private String id;
    private String description;
    private String address;
    private String url;
    private String title;
    @SerializedName("source")
    private GetSetSource getSetSource;

    public GetSetData(String id, String description, String address, String url, String title, GetSetSource getSetSource) {
        this.id = id;
        this.description = description;
        this.address = address;
        this.url = url;
        this.title = title;
        this.getSetSource = getSetSource;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public GetSetSource getGetSetSource() {
        return getSetSource;
    }

    public void setGetSetSource(GetSetSource getSetSource) {
        this.getSetSource = getSetSource;
    }
}
