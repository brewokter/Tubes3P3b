package com.example.petakecelakaansepeda;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {

    private SharedPreferences prefs;

    public Session(Context cntx) {
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setPage(int page) {
        prefs.edit().putInt("page", page).commit();
    }

    public int getPage() {
        int page = prefs.getInt("page",1);
        return page;
    }

    public void setIncident(String incident) {
        prefs.edit().putString("incident", incident).commit();
    }

    public String getIncident() {
        String incident = prefs.getString("incident","crash");
        return incident;
    }
}
