package com.example.petakecelakaansepeda;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListIncidentTypeActivity extends AppCompatActivity {

    AdapterData adapterData;
    List<GetSetData> getSetList;
    RecyclerView recyclerView;
    Button button_search;
    Spinner spinner_incident_type;


    Session session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_incident_type);

        getSetList = new ArrayList<>();
        session = new Session(this);

        String[] arraySpinner = new String[] {
                "crash", "hazard", "theft", "unconfirmed", "infrastructure_issue", "chop_shop"
        };

        spinner_incident_type = findViewById(R.id.spinner_incident_type);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_incident_type.setAdapter(adapter);

        button_search = findViewById(R.id.button_search);
        button_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setIncident(spinner_incident_type.getSelectedItem().toString());
                startActivity(new Intent(ListIncidentTypeActivity.this, ListIncidentTypeActivity.class));
                finish();
            }
        });

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(ListIncidentTypeActivity.this));
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(ListIncidentTypeActivity.this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        int itemPosition = recyclerView.getChildLayoutPosition(view);
                        final GetSetData item = getSetList.get(itemPosition);
                        Toast.makeText(ListIncidentTypeActivity.this, item.getGetSetSource().getHtml_url(), Toast.LENGTH_LONG).show();
                            /*Intent intent = new Intent(ListActivity.this, ListActivity.class);
                            intent.putExtra("id", item.getId());
                            intent.putExtra("url", item.getUrl());
                            startActivity(intent);
                            finish();*/
                    }

                    @Override public void onLongItemClick(View view, int position) {

                    }
                })
        );

        Retrofit retrofit = new Builder()
                .baseUrl("http://bikewise.org/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        JsonPlaceHolderJsonParamsIncidentsType jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderJsonParamsIncidentsType.class);

        //Call<List<GetSetData>> call = jsonPlaceHolderApi.getPosts();
        Call<JsonResponse> call = jsonPlaceHolderApi.getJson(
                session.getIncident()
        );

        call.enqueue(new Callback<JsonResponse>() {
            @Override
            public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                if(!response.isSuccessful()){
                    return;
                }

                JsonResponse posts = response.body();
                getSetList.addAll(Arrays.asList(posts.getBikeArray()));

                adapterData = new AdapterData(getSetList, ListIncidentTypeActivity.this);
                recyclerView.setAdapter(adapterData);
            }

            @Override
            public void onFailure(Call<JsonResponse> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ListIncidentTypeActivity.this, MainActivity.class));
        finish();
    }
}
