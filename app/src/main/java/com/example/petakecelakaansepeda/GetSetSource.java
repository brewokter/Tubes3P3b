package com.example.petakecelakaansepeda;

import com.google.gson.annotations.SerializedName;

public class GetSetSource {

    @SerializedName("html_url")
    private String html_url;

    public String getHtml_url() {
        return html_url;
    }

    public GetSetSource(String html_url) {
        this.html_url = html_url;
    }
}

