package com.example.petakecelakaansepeda;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterData extends RecyclerView.Adapter<AdapterData.ViewHolder> {

    private List<GetSetData> itemLists;
    private Context context;

    public AdapterData(List<GetSetData> itemLists, Context context){
        this.itemLists = itemLists;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_data, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GetSetData getSetItem = itemLists.get(position);

        holder.id_title.setText(getSetItem.getTitle());
        holder.id_description.setText(getSetItem.getDescription());
    }

    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView id_title, id_description;

        public ViewHolder(View view){
            super(view);

            id_title = view.findViewById(R.id.id_title);
            id_description = view.findViewById(R.id.id_description);
        }
    }
}
