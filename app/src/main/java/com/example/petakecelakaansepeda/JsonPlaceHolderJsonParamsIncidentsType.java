package com.example.petakecelakaansepeda;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceHolderJsonParamsIncidentsType {

    @GET("incidents")
    Call<JsonResponse> getJson(
            @Query("incident_type") String incident_type
    );
}
