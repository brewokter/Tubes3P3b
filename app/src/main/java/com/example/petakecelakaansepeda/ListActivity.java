package com.example.petakecelakaansepeda;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.lang.Thread.sleep;

public class ListActivity extends AppCompatActivity {

    AdapterData adapterData;
    List<GetSetData> getSetList;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        getSetList = new ArrayList<>();

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(ListActivity.this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        int itemPosition = recyclerView.getChildLayoutPosition(view);
                        final GetSetData item = getSetList.get(itemPosition);
                        Toast.makeText(ListActivity.this, item.getGetSetSource().getHtml_url(), Toast.LENGTH_LONG).show();
                            /*Intent intent = new Intent(ListActivity.this, ListActivity.class);
                            intent.putExtra("id", item.getId());
                            intent.putExtra("url", item.getUrl());
                            startActivity(intent);
                            finish();*/
                    }

                    @Override public void onLongItemClick(View view, int position) {

                    }
                })
        );

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://bikewise.org/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        JsonPlaceHolderJson jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderJson.class);

        //Call<List<GetSetData>> call = jsonPlaceHolderApi.getPosts();
        Call<JsonResponse> call = jsonPlaceHolderApi.getJson();

        call.enqueue(new Callback<JsonResponse>() {
            @Override
            public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {
                if(!response.isSuccessful()){
                    return;
                }

                JsonResponse posts = response.body();
                getSetList.addAll(Arrays.asList(posts.getBikeArray()));

                adapterData = new AdapterData(getSetList, ListActivity.this);
                recyclerView.setAdapter(adapterData);
            }

            @Override
            public void onFailure(Call<JsonResponse> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ListActivity.this, MainActivity.class));
        finish();
    }
}