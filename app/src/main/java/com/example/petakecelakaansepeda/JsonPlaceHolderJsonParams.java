package com.example.petakecelakaansepeda;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceHolderJsonParams {

    @GET("incidents")
    Call<JsonResponse> getJson(
            @Query("page") int page
    );
}
