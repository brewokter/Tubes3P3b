package com.example.petakecelakaansepeda;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolderJson {

    @GET("incidents")
    Call<JsonResponse> getJson();
}
