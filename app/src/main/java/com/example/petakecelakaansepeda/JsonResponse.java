package com.example.petakecelakaansepeda;

import com.google.gson.annotations.SerializedName;

public class JsonResponse {

    @SerializedName("incidents")
    private GetSetData[] bikeArray;

    public JsonResponse(GetSetData[] bikeArray) {
        this.bikeArray = bikeArray;
    }

    public GetSetData[] getBikeArray() {
        return bikeArray;
    }

    public void setBikeArray(GetSetData[] bikeArray) {
        this.bikeArray = bikeArray;
    }
}
